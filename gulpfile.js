const gulp = require('gulp')
const sass = require('gulp-sass')
const rename = require('gulp-rename')

gulp.task('minScss', () => {
  return gulp.src('./lib/index.scss')
    .pipe(rename('quickly-css.min.scss'))
    .pipe(sass({
        outputStyle: 'compressed'
    }))
    .pipe(gulp.dest('./dist'))
})

gulp.task('scss', () => {
  return gulp.src('./lib/index.scss')
    .pipe(rename('quickly-css.scss'))
    .pipe(sass({
        outputStyle: 'expanded'
    }))
    .pipe(gulp.dest('./dist'))
})

gulp.task('default', gulp.parallel('scss', 'minScss'))
